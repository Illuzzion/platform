from typing import Any, Sequence, Optional

from django.contrib import admin
from django.http import HttpRequest
from django.utils.translation import gettext_lazy as _

from .models.ledger import Ledger
from .models.tariff_plan import TariffPlan
from .models.user_financial_account import UserFinancialAccount
from .services.user_finance_svc import replenishment_balance_by_administrator


@admin.register(TariffPlan)
class TariffPlanAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "max_active_users",
        "disk_space_limit",
        "tariff_cost_per_day",
    )
    search_fields = ("title",)

    def has_add_permission(self, request):
        """Только админы могут создавать тарифы"""
        return True if request.user.is_superuser else False


@admin.register(UserFinancialAccount)
class UserFinanceAdmin(admin.ModelAdmin):
    fields = "id", "user", "user_tariff", "balance"
    list_display = "user", "user_tariff", "balance"
    list_filter = ("user_tariff",)
    empty_value_display = _("not selected")
    autocomplete_fields = ("user_tariff",)

    def get_readonly_fields(
            self, request: HttpRequest, obj: Optional[UserFinancialAccount] = ...
    ) -> Sequence[str]:
        return "id", "user", "balance"

    def has_add_permission(self, obj=None):
        """Нельзя добавлять через админку"""
        return False

    def has_delete_permission(self, request, obj=None):
        """Нельзя удалять финансы через админку"""
        return False


@admin.register(Ledger)
class LedgerAdmin(admin.ModelAdmin):
    list_display = (
        "subject",
        "amount",
        "success",
        "operation",
        "payment_date",
    )
    date_hierarchy = "payment_date"
    list_filter = "operation", "success"
    readonly_fields = "id", "payment_date"
    fields = (
        "id",
        "subject",
        "amount",
        "success",
        "operation",
        "payment_date",
        "operation_details",
    )
    autocomplete_fields = ("subject",)

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return "id", "payment_date", "subject", "amount", "operation", "success"
        return super().get_readonly_fields(request, obj)

    def get_fields(self, request, obj=None):
        fields = super().get_fields(request, obj)

        if obj:
            return fields

        remove_fields = "operation", "success", "payment_date"
        return [field for field in fields if field not in remove_fields]

    @staticmethod
    def _get_payment_details(request) -> str:
        meta = request.META
        ip, user_agent = meta.get("REMOTE_ADDR"), meta.get("HTTP_USER_AGENT")
        return f"user={request.user}\n{ip=}\n{user_agent}"

    def save_model(self, request: Any, obj: Ledger, form: Any, change: Any) -> None:
        """
        Было через сигналы, иногда они вызываются несколько раз
        вызовем явно
        """
        replenishment_balance_by_administrator(
            user=obj.subject,
            amount=obj.amount,
            operation_details=self._get_payment_details(request),
        )
