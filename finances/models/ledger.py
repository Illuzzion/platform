import uuid

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _


class PaymentOperation(models.TextChoices):
    ADM_INC = "ADMIN_INC", _("Replenishment of the balance by the administrator")
    TARIFF_EXP = "TARIFF_EXP", _("Tariff plan costs")


class PaymentQuerySet(models.QuerySet):
    """
    QuerySet который позволяет:
    - выбрать платежи указанного пользователя
    """

    def for_user(self, user: User):
        return self.filter(user=user)


class SuccessfulPaymentsManager(models.Manager):
    """
    Менеджер который:
    - использует PaymentQuerySet
    - выводит только успешные платежи
    """

    def get_queryset(self):
        """Только успешные платежи"""
        qs = PaymentQuerySet(self.model, using=self.db)
        return qs.filter(success=True)


class Ledger(models.Model):
    """
    Платежи и расходы
    Бухгалтерская книга
    """

    id = models.UUIDField(_("Id"), default=uuid.uuid4, primary_key=True)
    subject = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="user_payments",
        verbose_name=_("Subject"),
        help_text=_("Operation subject"),
    )

    success = models.BooleanField(
        _("Success?"),
        default=False,
        help_text=_("The success of the financial operation"),
    )

    operation = models.CharField(
        _("Operation"),
        choices=PaymentOperation.choices,
        max_length=20,
        help_text=_("Financial operation"),
    )

    operation_details = models.TextField(
        _("Operation details"), blank=True, help_text=_("Financial operation details")
    )

    amount = models.PositiveIntegerField(_("Amount"))
    payment_date = models.DateTimeField(_("Payment date"), auto_now_add=True)

    objects = PaymentQuerySet.as_manager()
    success_operations = SuccessfulPaymentsManager()

    class Meta:
        db_table = 'content"."ledger'
        ordering = "payment_date", "amount"
        verbose_name = _("Ledger")
        verbose_name_plural = _("Ledger records")

    def __str__(self):
        return f"{self.subject} {self.amount}"

    def mark_as_successful(self) -> None:
        """Пометим платеж как успешный"""
        self.success = True
        self.save(update_fields=("success",))
