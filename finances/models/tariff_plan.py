from django.db import models
from django.utils.translation import gettext_lazy as _


class TariffPlan(models.Model):
    """
    Тарифный план
    - название
    - лимиты
    - стоимость тарифа в день
    """

    title = models.CharField(_("Title"), max_length=100)
    max_organization_count = models.PositiveIntegerField(
        _("Maximum number of organizations"),
        default=1,
        help_text=_("The maximum number of organizations a user can create"),
    )

    max_active_users = models.PositiveIntegerField(_("Maximum number of active users"))
    disk_space_limit = models.PositiveIntegerField(
        _("Maximum disk space usage quota"),
        help_text=_("Maximum disk space usage quota, in megabytes"),
    )

    tariff_cost_per_day = models.PositiveIntegerField(
        _("Cost of the tariff"), help_text=_("The cost of the tariff per day")
    )

    class Meta:
        db_table = 'content"."tariff_plans'
        ordering = "title", "tariff_cost_per_day"
        verbose_name = _("Tariff plan")
        verbose_name_plural = _("Tariff plans")

    def __str__(self):
        return self.title
