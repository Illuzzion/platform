import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _


from finances.models.tariff_plan import TariffPlan


class UserFinancialAccount(models.Model):
    """
    Финансовые данные пользователя
    - тарифный план
    - баланс
    """

    id = models.UUIDField(_("Id"), default=uuid.uuid4, primary_key=True)

    user = models.OneToOneField(
        "auth.User",
        related_name="finances",
        on_delete=models.CASCADE,
        verbose_name=_("User"),
    )

    user_tariff = models.ForeignKey(
        TariffPlan,
        on_delete=models.SET_NULL,
        null=True,
        related_name="finances",
        verbose_name=_("Selected tariff plan"),
        help_text=_("Selected user tariff plan"),
    )

    balance = models.IntegerField(_("balance"), default=0)

    last_update = models.DateTimeField(_("Last Update"), auto_now=True)

    class Meta:
        db_table = 'content"."users_financial_accounts'
        ordering = "user", "user_tariff", "balance"
        verbose_name = _("User Financial Account")
        verbose_name_plural = _("Users Financial Accounts")

    def __str__(self):
        return "%(user)s, %(tariff)s %(user_tariff)s, %(balance_str)s %(balance)d" % {
            "user": self.user,
            "tariff": _("tariff"),
            "user_tariff": self.user_tariff,
            "balance_str": _("balance"),
            "balance": self.balance,
        }
