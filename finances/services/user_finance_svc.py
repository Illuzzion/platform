import logging

from django.contrib.auth import get_user_model
from django.db import transaction, IntegrityError

from ..exceptions import BadAmountError
from ..models.ledger import Ledger, PaymentOperation
from ..models.user_financial_account import UserFinancialAccount

User = get_user_model()
logger = logging.getLogger(__name__)


def get_user_balance(user: User) -> int:
    """Получение баланса пользователя"""
    user_finance, created = UserFinancialAccount.objects.get_or_create(user=user)
    return user_finance.balance


def _add_to_user_balance(subject: User, amount: int) -> UserFinancialAccount:
    """Пополнение баланса пользователя"""
    user_finance, created = UserFinancialAccount.objects.get_or_create(user=subject)
    user_finance.balance += amount
    user_finance.save(update_fields=("balance",))
    return user_finance


def _withdraw_from_user_balance(subject: User, amount: int) -> UserFinancialAccount:
    """Снятие денег с баланса"""
    user_finance, created = UserFinancialAccount.objects.get_or_create(user=subject)

    if user_finance.balance < amount:
        raise IntegrityError

    user_finance.balance -= amount
    user_finance.save(update_fields=("balance",))
    return user_finance


def _check_positive_amount(amount: int) -> bool:
    """Помощь при проверке входящей суммы"""
    if amount <= 0:
        raise BadAmountError(f"amount is {amount}")
    return True


def incoming_payment(
    user: User, amount: int, payment_operation, **kwargs
) -> UserFinancialAccount:
    """Входящий платеж"""
    _check_positive_amount(amount)

    payment = Ledger.objects.create(
        subject=user, operation=payment_operation, amount=amount, **kwargs
    )

    try:
        with transaction.atomic():
            user_financial_account = _add_to_user_balance(user, amount=amount)
            payment.mark_as_successful()
    except IntegrityError:
        logger.exception("Error on replenishment of user balance")
    else:
        return user_financial_account


def replenishment_balance_by_administrator(
    user: User, amount: int, **kwargs
) -> UserFinancialAccount:
    """Пополнение баланса от имени администратора"""
    return incoming_payment(
        user=user, amount=amount, payment_operation=PaymentOperation.ADM_INC, **kwargs
    )


def outgoing_payment(
    user: User, amount: int, payment_operation, **kwargs
) -> UserFinancialAccount:
    """Платеж с баланса"""
    _check_positive_amount(amount)

    spending = Ledger.objects.create(
        subject=user, operation=payment_operation, amount=amount, **kwargs
    )

    try:
        with transaction.atomic():
            user_financial_account = _withdraw_from_user_balance(user, amount)
            spending.mark_as_successful()
    except IntegrityError:
        logger.exception("Error on outgoing payment")
    else:
        return user_financial_account
