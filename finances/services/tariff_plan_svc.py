import logging
from dataclasses import dataclass

from django.conf import settings
from django.utils.translation import gettext_lazy

from .user_finance_svc import get_user_balance
from ..exceptions import TariffError
from ..models.tariff_plan import TariffPlan
from ..models.user_financial_account import UserFinancialAccount

logger = logging.getLogger(__name__)

User = settings.AUTH_USER_MODEL


def get_tariff_plan(user: User) -> TariffPlan | None:
    """Получим тарифный план пользователя или None, если тариф не выбран"""
    try:
        return TariffPlan.objects.get(finances__user=user)
    except TariffPlan.DoesNotExist:
        return None


def set_tariff_plan_for_user(user: User, tariff: TariffPlan) -> UserFinancialAccount:
    """
    У пользователя
      - создание записи UserFinance
      - Установка тарифного плана
    """

    instance, created = UserFinancialAccount.objects.get_or_create(
        user=user, defaults={"user_tariff": tariff}
    )

    if instance.user_tariff is not tariff:
        instance.user_tariff = tariff
        instance.save()

    return instance


@dataclass
class TariffPlanService:
    user: User
    _tariff_plan: TariffPlan = None
    _financial_account: UserFinancialAccount = None

    @property
    def tariff_plan(self) -> TariffPlan:
        """Получим тарифный план"""
        if not self._tariff_plan:
            try:
                self._tariff_plan = get_tariff_plan(self.user)
            except TariffPlan.DoesNotExist:
                logger.exception(gettext_lazy("TariffPlanService.tariff_plan property"))
                raise TariffError(gettext_lazy("TariffPlan for user does not exists"))
            except Exception:
                logger.exception(gettext_lazy("TariffPlanService.tariff_plan"))

        return self._tariff_plan

    @staticmethod
    def add_new_tariff_plan(
        title: str, max_active_users: int, disk_space_limit: int, cost_per_day: int
    ) -> TariffPlan:
        """
        Создание тарифного плана или получение существующего с одинаковыми параметрами
        """
        tariff, created = TariffPlan.objects.get_or_create(
            max_active_users=max_active_users,
            disk_space_limit=disk_space_limit,
            tariff_cost_per_day=cost_per_day,
            defaults={"title": title},
        )
        logger.info(gettext_lazy("TariffPlan created %(tariff)s" % {"tariff": tariff}))
        return tariff

    def can_user_create_another_organization(self) -> bool:
        """Проверка может ли пользователь создать еще 1 организацию"""
        if self.user.is_superuser:
            return True

        conditions = [
            not self._user_has_reached_organization_limit(),
            self._user_has_positive_balance(),
        ]
        return all(conditions)

    def _user_has_reached_organization_limit(self) -> bool:
        """
        Проверка достиг ли пользователь лимита на количество организаций, по тарифу?
        У суперпользователя лимита нет
        """
        from core.services.organization_svc import user_has_reached_organization_limit

        conditions = [user_has_reached_organization_limit(self.user)]

        can_create_organization = any(conditions)
        return not can_create_organization

    def _user_has_positive_balance(self) -> bool:
        """
        У пользователя положительный баланс?
        Пользователь админ?
        """
        balance = get_user_balance(self.user)

        conditions = [
            # self.user.is_superuser,
            balance > 0,
        ]
        return any(conditions)
