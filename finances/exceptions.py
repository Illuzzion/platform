class TariffError(Exception):
    def __str__(self):
        return "The user has not selected a tariff plan"


class BadAmountError(Exception):
    def __str__(self):
        return "You need to replenish the user's balance"
