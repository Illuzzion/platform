import pytest
from django.db import IntegrityError

from finances.exceptions import BadAmountError
from finances.models import PaymentOperation, Ledger
from finances.services.user_finance_svc import (
    incoming_payment,
    get_user_balance,
    _withdraw_from_user_balance,
    outgoing_payment,
)

pytestmark = [pytest.mark.django_db]


@pytest.mark.parametrize("amount", [0, -10])
def test_incoming_payment_bad_amount(user_with_tariff_and_balance, amount):
    """Не верные суммы платежа"""
    with pytest.raises(BadAmountError):
        incoming_payment(user_with_tariff_and_balance, amount, PaymentOperation.ADM_INC)


def test_incoming_payment_for_user(user):
    """Пополнение баланса пользователю у которого не выбран тариф"""
    assert get_user_balance(user) == 0

    payment_amount = 100
    incoming_payment(user, payment_amount, PaymentOperation.ADM_INC)
    assert get_user_balance(user) == payment_amount
    assert Ledger.success_operations.count() == 1


def test_withdraw_from_user_balance(user_with_tariff_and_balance):
    """
    Простой исходящий платеж
    через withdraw_from_user_balance
    """
    balance = get_user_balance(user_with_tariff_and_balance)
    assert balance > 0
    spending_amount = 11
    _withdraw_from_user_balance(user_with_tariff_and_balance, spending_amount)

    assert balance - spending_amount == get_user_balance(user_with_tariff_and_balance)


def test_withdraw_from_user_balance_excess_amount(user_with_tariff_and_balance):
    """
    Списываем сумму бОльшую, чем есть балансе
    через withdraw_from_user_balance
    """
    balance = get_user_balance(user_with_tariff_and_balance)
    assert balance > 0
    spending_amount = balance + 111
    with pytest.raises(IntegrityError):
        _withdraw_from_user_balance(user_with_tariff_and_balance, spending_amount)


def test_outgoing_payment(user_with_tariff_and_balance):
    """
    Простой исходящий платеж
    через outgoing_payment
    """
    balance = get_user_balance(user_with_tariff_and_balance)
    assert balance > 0, "Пополни баланс"
    spending_amount = 22
    outgoing_payment(
        user_with_tariff_and_balance, spending_amount, PaymentOperation.TARIFF_EXP
    )
    assert balance - spending_amount == get_user_balance(user_with_tariff_and_balance)


def test_outgoing_payment_excess_amount(user_with_tariff_and_balance):
    balance = get_user_balance(user_with_tariff_and_balance)
    assert balance > 0
    spending_amount = balance + 111
    assert Ledger.objects.filter(success=False).count() == 0
    outgoing_payment(
        user_with_tariff_and_balance, spending_amount, PaymentOperation.TARIFF_EXP
    )

    # проверим что транзакция откатилась и баланс не списался,
    # появилась запись о попытке неудачном списании
    assert get_user_balance(user_with_tariff_and_balance) == balance
    assert Ledger.objects.filter(success=False).count() == 1
