import pytest

from finances.models import TariffPlan
from finances.services.tariff_plan_svc import TariffPlanService

pytestmark = [pytest.mark.django_db]


def test_create_tariff():
    users, disk_space, cost_per_day = 3, 100, 100
    tp_svc = TariffPlanService
    t = tp_svc.add_new_tariff_plan(
        title="Basic",
        max_active_users=users,
        disk_space_limit=disk_space,
        cost_per_day=cost_per_day,
    )
    assert isinstance(t, TariffPlan)
    assert t.max_active_users == users
    assert t.disk_space_limit == disk_space
    assert t.tariff_cost_per_day == cost_per_day


@pytest.mark.params(title="NewBasic")
def test_tariff_fixture(basic_tariff):
    assert basic_tariff.title == "NewBasic"
