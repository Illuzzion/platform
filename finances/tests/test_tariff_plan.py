from random import choice

import pytest
from django.contrib.auth.models import User

from core.models import Organization
from ..models import TariffPlan, UserFinancialAccount
from ..services.tariff_plan_svc import TariffPlanService, set_tariff_plan_for_user
from ..services.user_finance_svc import (
    replenishment_balance_by_administrator,
    get_user_balance,
)

pytestmark = [pytest.mark.django_db]


def test_set_tariff_for_user(user: User, basic_tariff: TariffPlan) -> None:
    """Проверка установки тарифа для пользователя"""
    user_finance: UserFinancialAccount = set_tariff_plan_for_user(user, basic_tariff)
    assert user_finance.user == user
    assert user_finance.user_tariff == basic_tariff
    assert user_finance.balance == 0


def test_get_user_tariff(
    user: User, basic_tariff: TariffPlan, advanced_tariff: TariffPlan
) -> None:
    """Проверка функции get_user_tariff_plan"""
    selected_tp = choice([basic_tariff, advanced_tariff])

    set_tariff_plan_for_user(user, selected_tp)
    tp_svc = TariffPlanService(user)
    tp = tp_svc.tariff_plan
    assert isinstance(tp, TariffPlan)

    assert tp == selected_tp


def test_replenishment_of_user_balance(user: User, basic_tariff: TariffPlan) -> None:
    """Проверка пополнения баланса пользователя"""
    # user_finance: UserFinancialAccount = set_tariff_plan_for_user(user, basic_tariff)
    assert get_user_balance(user) == 0

    amount_value = 123
    replenishment_balance_by_administrator(user, amount_value)
    assert get_user_balance(user) == amount_value


def test_user_has_reached_organization_limit(
    user: User, organization: Organization, basic_tariff: TariffPlan
):
    """
    Проверка метода сервиса
    _user_has_reached_organization_limit

    Во время создания организации создается и master_user,
    потому что, организации не могут быть бесхозными
    """
    master_user = organization.master_user
    tp_svc = TariffPlanService(master_user)
    set_tariff_plan_for_user(master_user, basic_tariff)
    assert tp_svc._user_has_reached_organization_limit()


def test_user_has_not_reached_organization_limit(user: User, basic_tariff: TariffPlan):
    """
    Проверка метода сервиса
    _user_has_reached_organization_limit
    что пользователь не достиг лимита организаций
    """
    tp_svc = TariffPlanService(user)
    set_tariff_plan_for_user(user, basic_tariff)
    assert tp_svc._user_has_reached_organization_limit() is False


def test_user_has_positive_balance(user):
    tp_svc = TariffPlanService(user)
    assert tp_svc._user_has_positive_balance() is False
