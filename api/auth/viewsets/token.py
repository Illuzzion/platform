from rest_framework_simplejwt.views import TokenObtainPairView

from api.auth.serializers.token import CustomTokenObtainPairSerializer


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer
