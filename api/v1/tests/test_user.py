import pytest
from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken

from core.services.permissions_svc import grant_olp_permissions

list_url = reverse("user-list")

pytestmark = pytest.mark.django_db
User = get_user_model()


class TestJWTUser:
    def test_user_list_jwt(self, api_client, user):
        """
        Проверка получения списка пользователей,
        на которых есть права на просмотр установленные через grant_olp_permissions().

        Для авторизации используется JWT токен.
        """
        grant_olp_permissions(user, user, ("view_user",))

        tokens = RefreshToken.for_user(user)
        api_client.credentials(HTTP_AUTHORIZATION=f"Bearer {tokens.access_token}")

        response = api_client.get(list_url)
        assert response.status_code == 200

        data = response.data
        assert data["count"] == 1

        results = data["results"]
        assert len(results) == 1
        user_dict = results[0]
        assert user_dict["id"] == user.id
        # поле из профиля и оно не заполнено
        assert not user_dict["middleName"]


class TestUser:
    def test_retrieve_user_detail(self, api_client: APIClient, user: User) -> None:
        grant_olp_permissions(
            who_gets_permissions=user,
            object_of_permission=user,
            mlp_actions=("view_user",),
        )

        tokens = RefreshToken.for_user(user)
        api_client.credentials(HTTP_AUTHORIZATION=f"Bearer {tokens.access_token}")

        detail_url = reverse("user-detail", kwargs={"pk": user.id})
        response = api_client.get(detail_url)

        user_dict = response.data
        assert user_dict["id"] == user.id


def test_change_yourself(client, user):
    """
    Пробуем заменить поле
    `username` - не меняется
    """

    # Здесь не хватает прав на просмотр объекта и авторизации по токену
    client.force_login(user)
    detail_url = reverse("user-detail", kwargs={"pk": user.id})
    new_username = user.username * 2
    response = client.patch(
        detail_url, data={"firstName": new_username}, content_type="application/json"
    )
    assert response.status_code == 200
    assert response.data["firstName"] == new_username


def test_change_profile_field(client, user):
    # Здесь не хватает прав на просмотр объекта и авторизации по токену
    client.force_login(user)
    detail_url = reverse("user-detail", kwargs={"pk": user.id})
    new_middle_name = user.username * 2

    response = client.patch(
        detail_url,
        data={"middleName": new_middle_name},
        content_type="application/json",
    )
    assert response.status_code == 200

    response = client.get(detail_url)
    assert response.status_code == 200
    assert response.data["middleName"] == new_middle_name
