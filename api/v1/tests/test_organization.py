import pytest
from django.urls import reverse

from core.models import Organization
from core.services.permissions_svc import grant_olp_actions

pytestmark = pytest.mark.django_db

list_url = reverse("organization-list")


def test_organization_list_anonymous(client, many_organizations):
    """Попытка просмотра всех организаций, не авторизованным пользователем"""
    response = client.get(list_url)
    assert response.status_code == 401


def test_organization_list_authorized(client, user, many_organizations):
    """Попытка просмотра всех организаций, без права на просмотр"""
    client.force_login(user)
    response = client.get(list_url)
    assert response.status_code == 200
    data = response.data
    assert data["count"] == 0


def test_organization_list_master_user_authorized(
    client, many_organizations, organization
):
    """Просмотр списка своих организаций"""
    user = organization.master_user

    client.force_login(user)
    response = client.get(list_url)
    assert response.status_code == 200

    data = response.data
    # узнаем сколько всего организаций в базе
    organizations_in_db = Organization.objects.count()
    assert organizations_in_db > 1
    # количество организаций в базе, больше количества, которое видит пользователь
    assert data["count"] < organizations_in_db


def test_organization_view_allowed(client, organization):
    """Просмотр организации по id"""
    user = organization.master_user

    client.force_login(user)
    detail_url = reverse("organization-detail", kwargs={"pk": organization.id})
    response = client.get(detail_url)
    assert response.status_code == 200


def test_organization_change_not_allowed(client, organization):
    """
    Попытка изменить поле организации,
    когда доступ запрещен
    """
    user = organization.master_user
    client.force_login(user)
    detail_url = reverse("organization-detail", kwargs={"pk": organization.id})
    new_title = "New title"
    response = client.patch(
        detail_url, data={"title": new_title}, content_type="application/json"
    )
    assert response.status_code == 403


def test_organization_change_allowed(client, organization):
    """
    Попытка изменить поле организации,
    когда доступ разрешен
    """
    user = organization.master_user
    detail_url = reverse("organization-detail", kwargs={"pk": organization.id})
    client.force_login(user)

    grant_olp_actions(user, organization, ["change"])

    new_title = "New title"
    response = client.patch(
        detail_url, data={"title": new_title}, content_type="application/json"
    )
    assert response.status_code == 200
    assert response.data["title"] == new_title


def test_organization_change_admin_user(organization, admin_client):
    """
    Попытка изменить поле организации
    от имени администратора
    """
    detail_url = reverse("organization-detail", kwargs={"pk": organization.id})
    new_title = "New title"

    response = admin_client.patch(
        detail_url, data={"title": new_title}, content_type="application/json"
    )
    assert response.status_code == 200
