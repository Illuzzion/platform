from rest_framework import routers

from .viewsets.organization import OrganizationViewSet
from .viewsets.user import UserViewSet

api_router = routers.DefaultRouter()
api_router.register("organization", OrganizationViewSet, basename="organization")
api_router.register("user", UserViewSet, basename="user")

urlpatterns: list = api_router.urls
