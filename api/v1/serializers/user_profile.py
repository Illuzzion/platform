from rest_framework import serializers

from api.v1.serializers.profession import ProfessionSerializer
from core.models.user_profile import UserProfile


class UserProfileSerializer(serializers.ModelSerializer):
    profession = ProfessionSerializer()

    class Meta:
        model = UserProfile
        fields = (
            "middle_name",
            "phone",
            "birthday",
            "country",
            "city",
            "profession",
            "grade",
        )
