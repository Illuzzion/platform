from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.decorators import action

from .profession import ProfessionSerializer
from .user_profile import UserProfileSerializer


class UserSerializer(serializers.ModelSerializer):
    """Сериализатор пользователя"""

    firstName = serializers.CharField(source="first_name")
    lastName = serializers.CharField(source="last_name")
    middleName = serializers.CharField(
        source="profile.middle_name", allow_blank=True, default=""
    )
    phone = serializers.CharField(
        source="profile.phone", allow_null=True, allow_blank=True
    )
    birthday = serializers.DateField(source="profile.birthday", allow_null=True)
    country = serializers.CharField(
        source="profile.country", allow_null=True, allow_blank=True
    )
    city = serializers.CharField(
        source="profile.city", allow_null=True, allow_blank=True
    )
    profession = ProfessionSerializer(
        source="profile.profession", allow_null=True, read_only=True
    )
    grade = serializers.IntegerField(source="profile.grade", allow_null=True)

    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "email",
            "lastName",
            "firstName",
            "middleName",
            "phone",
            "birthday",
            "country",
            "city",
            "profession",
            "grade",
        )

        extra_kwargs = {"password": {"write_only": True}}

        read_only_fields = ("id", "username", "email")

    @staticmethod
    def _update_user_profile(
        nested_instance: UserProfileSerializer, nested_data: dict
    ) -> None:
        """Обновим вложенный профиль пользователя"""
        nested_serializer = UserProfileSerializer()
        nested_serializer.update(nested_instance, nested_data)

    def update(self, instance, validated_data):
        """
        Кастомный метод для обновления
        вложенного профиля пользователя
        """
        if "profile" in validated_data:
            self._update_user_profile(instance.profile, validated_data.pop("profile"))

        return super().update(instance, validated_data)

    @action(detail=False, methods=["GET", "PATCH"], url_path="me")
    def me(self, request, *args, **kwargs):
        """Информация о текущем пользователе"""
        self.kwargs.update({self.lookup_field: self.request.user.id})

        if request.method == "PATCH":
            return self.partial_update(request, *args, **kwargs)
        me = self.retrieve(request, *args, **kwargs)
        return me


class ShortUserSerializer(UserSerializer):
    active = serializers.BooleanField(source="is_active")

    def get_fields(self):
        fields = super().get_fields()
        exclude_fields = (
            # 'professions',
            # 'username',
            "birthdate",
            "country",
            "city",
        )
        return {
            name: value for name, value in fields.items() if name not in exclude_fields
        }
