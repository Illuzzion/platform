from rest_framework import serializers

from core.models.organization import Organization


class OrganizationSerializer(serializers.ModelSerializer):
    """
    Сериализатор модели Организация
    """

    class Meta:
        model = Organization
        fields = "id", "title"
