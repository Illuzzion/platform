from rest_framework import serializers

from core.models.profession import Profession


class ProfessionSerializer(serializers.ModelSerializer):
    """
    Сериализатор модели [Профессия](/core/models/#core.models.profession.Profession)
    """

    class Meta:
        model = Profession
        fields = (
            "id",
            "title",
        )
