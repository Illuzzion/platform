from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework_guardian import filters

from ..serializers.user import UserSerializer
from ...permissions import CombinedPermission


class UserViewSet(viewsets.ModelViewSet):
    """
    Список всех пользователей
    """

    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [CombinedPermission]
    filter_backends = [filters.ObjectPermissionsFilter]

    def list(self, request, *args, **kwargs):
        res = super().list(request, *args, **kwargs)
        return res

    def get_queryset(self):
        qs = super().get_queryset().order_by("id")
        return qs

    @action(detail=False, methods=["GET", "PATCH"], url_path="me")
    def me(self, request, *args, **kwargs):
        """Информация о текущем пользователе"""
        self.kwargs.update({self.lookup_field: self.request.user.id})

        if request.method == "PATCH":
            return self.partial_update(request, *args, **kwargs)

        me = self.retrieve(request, *args, **kwargs)
        return me
