from rest_framework import viewsets
from rest_framework_guardian import filters

from core.models.organization import Organization
from ..serializers.organization import OrganizationSerializer
from ...permissions import CombinedPermission


class OrganizationViewSet(viewsets.ModelViewSet):
    serializer_class = OrganizationSerializer
    queryset = Organization.objects.all()
    permission_classes = [CombinedPermission]
    filter_backends = [filters.ObjectPermissionsFilter]
