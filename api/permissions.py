"""
Модуль с классом определения прав доступа
"""
from django.core.exceptions import MultipleObjectsReturned
from django.http import HttpRequest
from rest_framework.permissions import DjangoModelPermissions

from core.services.permissions_svc import has_object_permissions


class CombinedPermission(DjangoModelPermissions):
    """
    Комбинация проверки 2-х уровней прав

    - уровень модели
    - уровень объекта
    """

    actions_map = {
        "GET": "view",
        "PUT": "change",
        "PATCH": "change",
        "DELETE": "delete",
    }

    def _has_object_permissions(self, request: HttpRequest, view) -> bool:
        """
        Внутренний метод для определения наличия прав доступа на уровне объекта
        """
        try:
            obj = view.queryset.get(**view.kwargs)
        except MultipleObjectsReturned:
            # Список на уровне OLP не интересен
            return False
        else:
            permission_action = self.actions_map.get(request.method)
            return has_object_permissions(request.user, obj, [permission_action])

    def has_permission(self, request: HttpRequest, view) -> bool:
        """Проверяем есть ли права на выбранное действие, у пользователя из запроса

        - Не пропускает анонимных пользователей
        - Если есть ли доступ к модели и `action=create`, то вернет права доступа к модели
        - Если дошли до сюда, то проверим доступ на уровне объекта

        Args:
            request: Обычный request Django
            view: нужно расписать детальнее

        Returns:
            bool: Есть ли доступ к текущему объекту/модели
        """
        # deny anonymous
        if not all([request.user, request.user.is_authenticated]):
            return False

        model_permission = super().has_permission(request, view)
        if any([model_permission, view.action == "create"]):
            return model_permission

        return self._has_object_permissions(request, view)
