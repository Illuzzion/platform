from rest_framework import permissions


class BaseActionPermissionMixin:
    default_permission = permissions.IsAuthenticated

    def get_default_permissions(self) -> list:
        """Метод для получения прав по умолчанию

        Returns:
            Список прав по умолчанию
        """
        return [self.default_permission()]

    def get_permissions(self) -> list:
        """
        Получение прав пользователя на объект

        Returns:
            Список разрешений
        """
        try:
            return [
                permission()
                for permission in self.permission_classes_by_action[self.action]
            ]
        except KeyError:
            # action не найден в списке permission_classes_by_action
            return [permission() for permission in self.permission_classes]
        except AttributeError:
            # в классе не найден список permission_classes_by_action
            return self.get_default_permissions()
