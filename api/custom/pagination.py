from rest_framework import pagination
from rest_framework.response import Response


# https://www.django-rest-framework.org/api-guide/pagination/#custom-pagination-styles
class CustomPagination(pagination.PageNumberPagination):
    def get_paginated_response(self, data):
        return Response(
            {
                "count": self.page.paginator.count,
                "pageSize": self.page_size,
                "results": data,
            }
        )
