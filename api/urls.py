from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from .auth import urls as auth_urls
from .v1 import urls as v1_urls

schema_view = get_schema_view(
    openapi.Info(
        title="Platform API",
        default_version='v1',
        description="Learning Platform API",
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path("auth/", include(auth_urls)),
    path("v1/", include(v1_urls)),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
