import pytest
from _pytest.mark import Mark
from django.contrib.auth.models import User, Group
from mixer.auto import mixer
from rest_framework.test import APIClient

from core.models import Organization
from core.services.organization_svc import create_organization
from finances.models.tariff_plan import TariffPlan


# default_mark = pytest.mark.parametrize("empty_mark", [Mark("params", (), {})])


@pytest.fixture()
def user() -> User:
    user = mixer.blend(User)
    return user


@pytest.fixture()
def group() -> Group:
    return mixer.blend(Group)


@pytest.fixture()
def empty_params_mark() -> Mark:
    return Mark("params", (), {})


@pytest.fixture()
def basic_tariff(request, empty_params_mark) -> TariffPlan:
    kwargs = dict(
        title="Basic",
        max_organization_count=1,
        max_active_users=3,
        disk_space_limit=1000,
    )
    fixture_params = request.node.get_closest_marker("params", empty_params_mark).kwargs
    fixture_params = fixture_params.get(request.fixturename, fixture_params)
    kwargs.update(fixture_params)

    tp = mixer.blend(TariffPlan, **kwargs)
    return tp


@pytest.fixture
def advanced_tariff() -> TariffPlan:
    return mixer.blend(
        TariffPlan,
        title="Advanced",
        max_organization_count=3,
        max_active_users=10,
        disk_space_limit=10000,
    )


@pytest.fixture()
def user_without_tariff_but_balance(user) -> User:
    """
    Пользователь с
    - выбранным тарифом
    - положительным балансом
    """
    from finances.services.user_finance_svc import (
        replenishment_balance_by_administrator,
    )

    replenishment_balance_by_administrator(user, amount=100)
    return user


@pytest.fixture()
def user_with_tariff_and_balance(user, basic_tariff) -> User:
    """
    Пользователь с
    - выбранным тарифом
    - положительным балансом
    """

    from finances.services.tariff_plan_svc import set_tariff_plan_for_user
    from finances.services.user_finance_svc import (
        replenishment_balance_by_administrator,
    )

    set_tariff_plan_for_user(user, basic_tariff)
    replenishment_balance_by_administrator(user, amount=100)
    return user


@pytest.fixture()
def organization(basic_tariff) -> Organization:
    """
    Вернем организацию
    - с выбранным тарифом
    - положительным балансом
    - пользователя брать в master_user
    """
    user = mixer.blend(User)
    from finances.services.tariff_plan_svc import set_tariff_plan_for_user
    from finances.services.user_finance_svc import (
        replenishment_balance_by_administrator,
    )

    set_tariff_plan_for_user(user, basic_tariff)
    replenishment_balance_by_administrator(user, amount=100)

    title = "Organization #0"
    org = create_organization(title=title, master_user=user)
    return org


@pytest.fixture
def many_organizations(admin_user):
    from core.models.organization import Organization

    mixer.cycle(10).blend(Organization, master_user=admin_user)


@pytest.fixture()
def api_client():
    return APIClient()
