start-docker:
	@echo "Starting containers"
	docker compose -f docker-compose.yml up -d

stop-docker:
	@echo "Stoping containers"
	docker compose -f docker-compose.yml down

docs-server:
	mkdocs serve
