# Документация `core`

- [Сигналы](signals/index.md)
- [Модели](models/index.md)
- [Сервисы](services/index.md)