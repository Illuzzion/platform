from django.db import models
from django.utils.translation import gettext_lazy as _


class Profession(models.Model):
    """
    Название профессии
    используется в профиле пользователя
    """

    title = models.CharField(_("Profession title"), max_length=100, unique=True)

    class Meta:
        db_table = 'content"."professions'
        verbose_name = _("Profession")
        verbose_name_plural = _("Professions")
        ordering = ("title",)

    def __str__(self):
        return self.title
