from django.contrib.auth.models import Group
from django.db import models
from django.utils.translation import gettext_lazy as _

from core.models.organization import Organization


class GroupType(models.TextChoices):
    """Выбор типа группы для организации"""

    STUDENTS = "STUDENTS", _("Group of students")
    MANAGERS = "MANAGERS", _("Group of managers")


class OrganizationGroup(models.Model):
    """
    Связь организации с группой,
    Нужна, чтобы находить группы у организации и наоборот
    для каждой организации должна быть по 1 группе каждого типа
    """

    organization = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
        verbose_name=_("Organization"),
        related_name="organization_groups",
        help_text=_("Organization associated with the group"),
    )

    group = models.OneToOneField(
        Group,
        on_delete=models.CASCADE,
        verbose_name=_("Group"),
        related_name="organization_groups",
        help_text=_("Group associated with the organization"),
    )

    type = models.CharField(
        max_length=32,
        choices=GroupType.choices,
        default=GroupType.STUDENTS,
        verbose_name=_("Group type"),
    )

    created = models.DateTimeField(_("Creation date"), auto_now_add=True)
    updated = models.DateTimeField(_("Update date"), auto_now=True)

    class Meta:
        db_table = 'content"."organization_groups'
        ordering = "organization", "type", "id"
        verbose_name = _("Organization Group")
        verbose_name_plural = _("Organization Groups")
        unique_together = ("organization", "type")

    def __str__(self):
        return f"{self.organization.title} {self.get_type_display()}"
