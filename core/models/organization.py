import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _
from django_lifecycle import LifecycleModelMixin, hook, BEFORE_CREATE, AFTER_CREATE

from core.services.permissions_svc import grant_mlp_actions, grant_olp_actions
from finances.exceptions import TariffError
from finances.services.tariff_plan_svc import TariffPlanService


class OrganizationManager(models.Manager):
    """
    Менеджер для выбора только активных организаций
    """

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(active=True)


class Organization(LifecycleModelMixin, models.Model):
    """
    Организация
    """

    id = models.UUIDField(default=uuid.uuid4, primary_key=True, unique=True)
    title = models.CharField(_("Title"), max_length=100, unique=True)
    active = models.BooleanField(_("Active?"), default=True)
    master_user = models.ForeignKey(
        "auth.User",
        on_delete=models.SET_NULL,
        null=True,
        verbose_name=_("Master user"),
        help_text=_("The user from whose balance the subscription fee will be charged"),
    )

    objects = models.Manager()
    only_active = OrganizationManager()

    class Meta:
        db_table = 'content"."organizations'
        ordering = (
            "title",
            "id",
        )
        verbose_name = _("Organization")
        verbose_name_plural = _("Organizations")

    def __str__(self):
        return self.title

    @hook(BEFORE_CREATE)
    def before_create_organization_hook(self):
        """Проверка может ли пользователь создать еще одну организацию"""
        tp_svc = TariffPlanService(self.master_user)

        if not tp_svc.can_user_create_another_organization():
            raise TariffError

    @hook(AFTER_CREATE)
    def after_create_organization(self):
        """
        Хук вызывается после создания организации

        - создаются группы студентов и менеджеров
        - группы связываются с организацией
        - группам выдаются права на модель и объект
        """
        from ..services.group_svc import (
            create_group,
            associate_organization_and_group,
            add_user_to_group,
            generate_group_name,
        )

        organization = self
        from core.models.organization_group import GroupType

        students_group_name = generate_group_name(organization, tail="-students")
        students_group = create_group(name=students_group_name)
        associate_organization_and_group(
            organization, students_group, GroupType.STUDENTS
        )

        managers_group_name = generate_group_name(organization, tail="-managers")
        managers_group = create_group(name=managers_group_name)
        associate_organization_and_group(
            organization, managers_group, GroupType.MANAGERS
        )

        add_user_to_group(organization.master_user, students_group)
        add_user_to_group(organization.master_user, managers_group)

        grant_mlp_actions(students_group, organization, ("view",))
        owner_allowed_actions = ("add", "view")
        grant_olp_actions(managers_group, organization, owner_allowed_actions)
