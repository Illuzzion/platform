from django.db import models
from django.utils.translation import gettext_lazy as _

from .profession import Profession


class UserProfile(models.Model):
    """Профиль пользователя"""

    user = models.OneToOneField(
        "auth.User",
        related_name="profile",
        on_delete=models.CASCADE,
        verbose_name=_("User"),
    )

    middle_name = models.CharField(_("Middle name"), max_length=100, blank=True)

    phone = models.CharField(_("Phone number"), max_length=15, blank=True)

    birthday = models.DateField(_("Birthday"), null=True, blank=True)

    country = models.CharField(_("Country"), max_length=75, blank=True)
    city = models.CharField(_("City"), max_length=75, blank=True)

    profession = models.ForeignKey(
        Profession, on_delete=models.SET_NULL, verbose_name=_("Profession"), null=True
    )
    grade = models.PositiveIntegerField(_("Grade"), blank=True, null=True)

    class Meta:
        db_table = 'content"."users_profiles'
        verbose_name = _("User profile")
        verbose_name_plural = _("Users profiles")
        ordering = ("user",)

    def __str__(self):
        full_name = self.user.get_full_name()
        display_name = full_name if full_name else self.user.username
        return display_name
