"""
Сигналы приложения `core`
"""
from typing import Type

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from guardian.utils import get_anonymous_user

from .models.user_profile import UserProfile
from .services.permissions_svc import grant_olp_actions


@receiver(post_save, sender="auth.User")
def create_self_permissions(
    sender: Type[User], instance: User, created: bool, *args, **kwargs
) -> None:
    """
    Вызывается после создания экземпляра модели `auth.User`:

    - создаем профиль
    - добавляем права на изменение информации своего пользователя и своего профиля

    Args:
        sender:
        instance: экземпляра модели `auth.User`
        created:
        *args:
        **kwargs:

    Returns:
        None: Ничего не возвращает
    """
    user = instance
    anonymous = get_anonymous_user()
    if user == anonymous:
        return

    if created:
        # создаем профиль
        user_profile = UserProfile.objects.create(user=user)
        # добавляем права на изменение информации своего пользователя и своего профиля
        grant_olp_actions(user, user, ("view", "change"))
        grant_olp_actions(user, user_profile, ("view", "change"))
