import pytest

from core.services.group_svc import get_organization_students_group, add_user_to_group
from core.services.user_svc import organizations_user_belongs_to

pytestmark = pytest.mark.django_db


def test_user_organization_list(organization, user, many_organizations) -> None:
    """
    В каких организациях состоит пользователь
    проверка organizations_user_belongs_to()
    """
    master_user = organization.master_user
    user_organizations = organizations_user_belongs_to(master_user)
    assert organization in user_organizations

    new_user_organizations = organizations_user_belongs_to(user)
    assert organization not in new_user_organizations

    students_group = get_organization_students_group(organization)
    add_user_to_group(user, students_group)
    new_user_organizations = organizations_user_belongs_to(user)
    assert new_user_organizations.count() == 1
    assert organization in new_user_organizations
