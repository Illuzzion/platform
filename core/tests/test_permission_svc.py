import pytest

from ..services.permissions_svc import (
    build_mlp_actions,
    build_olp_actions,
    grant_mlp_permissions,
    grant_olp_permissions,
    get_instance_type,
    grant_mlp_actions,
    grant_olp_actions,
    has_model_permissions,
    has_object_permissions,
)

pytestmark = [pytest.mark.django_db]


def test_build_mlp_actions(organization):
    model_permission_list = build_mlp_actions(organization, ["view"])
    assert model_permission_list == ["core.view_organization"]

    full_model_permission_list = build_mlp_actions(
        organization, ["view", "add", "change", "delete"]
    )
    assert full_model_permission_list == [
        "core.view_organization",
        "core.add_organization",
        "core.change_organization",
        "core.delete_organization",
    ]


def test_build_olp_actions(organization):
    obj_permission_list = build_olp_actions(organization, ["view"])
    assert obj_permission_list == ["view_organization"]

    full_object_permission_list = build_olp_actions(
        organization, ["view", "add", "change", "delete"]
    )

    assert full_object_permission_list == [
        "view_organization",
        "add_organization",
        "change_organization",
        "delete_organization",
    ]


def test_get_instance_type(user, group):
    assert (user, None) == get_instance_type(user)
    assert (None, group) == get_instance_type(group)
    assert (None, None) == get_instance_type(None)


@pytest.mark.parametrize(
    "allow_actions, deny_actions",
    [
        (["view"], ["add", "change", "delete"]),
        (["view", "add"], ["change", "delete"]),
        (["view", "add", "change"], ["delete"]),
        (["view", "add", "change", "delete"], []),
    ],
)
def test_grant_mlp_permissions(organization, user, allow_actions, deny_actions):
    requested_permissions = build_mlp_actions(
        organization, allow_actions, with_app_label=False
    )
    grant_mlp_permissions(user, organization, requested_permissions)

    f_actions = build_mlp_actions(organization, allow_actions)
    assert user.has_perms(f_actions)

    if not deny_actions:
        return

    f_deny_actions = build_mlp_actions(organization, deny_actions)
    assert user.has_perms(f_deny_actions) is False


@pytest.mark.parametrize(
    "allow_actions, deny_actions",
    [
        (["view"], ["add", "change", "delete"]),
        (["view", "add"], ["change", "delete"]),
        (["view", "add", "change"], ["delete"]),
        (["view", "add", "change", "delete"], []),
    ],
)
def test_grant_olp_permissions(organization, user, allow_actions, deny_actions):
    requested_permissions = build_olp_actions(organization, allow_actions)
    grant_olp_permissions(user, organization, requested_permissions)

    allowed_permissions = build_mlp_actions(organization, allow_actions)

    for permission in allowed_permissions:
        assert user.has_perm(permission, organization)

    if not deny_actions:
        return

    deny_permissions = build_mlp_actions(organization, deny_actions)
    for permission in deny_permissions:
        assert user.has_perm(permission, organization) is False


@pytest.mark.parametrize(
    "allow_actions, deny_actions",
    [
        (["view"], ["add", "change", "delete"]),
        (["view", "add"], ["change", "delete"]),
        (["view", "add", "change"], ["delete"]),
        (["view", "add", "change", "delete"], []),
    ],
)
def test_grant_mlp_actions(organization, user, allow_actions, deny_actions):
    grant_mlp_actions(user, organization, allow_actions)

    f_actions = build_mlp_actions(organization, allow_actions)
    assert user.has_perms(f_actions)

    if not deny_actions:
        return

    f_deny_actions = build_mlp_actions(organization, deny_actions)
    assert user.has_perms(f_deny_actions) is False


@pytest.mark.parametrize(
    "allow_actions, deny_actions",
    [
        (["view"], ["add", "change", "delete"]),
        (["view", "add"], ["change", "delete"]),
        (["view", "add", "change"], ["delete"]),
        (["view", "add", "change", "delete"], []),
    ],
)
def test_grant_olp_actions(organization, user, allow_actions, deny_actions):
    grant_olp_actions(user, organization, allow_actions)

    allowed_permissions = build_mlp_actions(organization, allow_actions)

    for permission in allowed_permissions:
        assert user.has_perm(permission, organization)

    if not deny_actions:
        return

    deny_permissions = build_mlp_actions(organization, deny_actions)
    for permission in deny_permissions:
        assert user.has_perm(permission, organization) is False


@pytest.mark.parametrize(
    "allow_actions, deny_actions",
    [
        (["view"], ["add", "change", "delete"]),
        (["view", "add"], ["change", "delete"]),
        (["view", "add", "change"], ["delete"]),
        (["view", "add", "change", "delete"], []),
    ],
)
def test_has_mlp_permission(user, organization, allow_actions, deny_actions):
    grant_mlp_actions(user, organization, allow_actions)
    assert has_model_permissions(user, organization, allow_actions)

    if deny_actions:
        assert has_model_permissions(user, organization, deny_actions) is False


@pytest.mark.parametrize(
    "allow_actions, deny_actions",
    [
        (["view"], ["add", "change", "delete"]),
        (["view", "add"], ["change", "delete"]),
        (["view", "add", "change"], ["delete"]),
        (["view", "add", "change", "delete"], []),
    ],
)
def test_user_has_olp_permission(user, organization, allow_actions, deny_actions):
    """
    Тест доступа
    - пользователю выдали права
    - проверяем права доступа от имени пользователя уровня olp
    """

    grant_olp_actions(user, organization, allow_actions)
    assert has_object_permissions(user, organization, allow_actions)

    if deny_actions:
        assert has_object_permissions(user, organization, deny_actions) is False


@pytest.mark.parametrize(
    "allow_actions, deny_actions",
    [
        (["view"], ["add", "change", "delete"]),
        (["view", "add"], ["change", "delete"]),
        (["view", "add", "change"], ["delete"]),
        (["view", "add", "change", "delete"], []),
    ],
)
def test_group_has_mlp_permission(
    user, group, organization, allow_actions, deny_actions
):
    """
    Тест доступа
    - группе выдали права
    - пользователя добавили в группу
    - проверяем права доступа от имени пользователя уровня mlp
    """
    grant_mlp_actions(group, organization, allow_actions)
    user.groups.add(group)
    assert user in group.user_set.all()
    assert has_model_permissions(user, organization, allow_actions)

    if deny_actions:
        assert has_model_permissions(user, organization, deny_actions) is False


@pytest.mark.parametrize(
    "allow_actions, deny_actions",
    [
        (["view"], ["add", "change", "delete"]),
        (["view", "add"], ["change", "delete"]),
        (["view", "add", "change"], ["delete"]),
        (["view", "add", "change", "delete"], []),
    ],
)
def test_group_has_olp_permission(
    group, user, organization, allow_actions, deny_actions
):
    """
    Тест доступа
    - группе выдали права
    - пользователя добавили в группу
    - проверяем права доступа от имени пользователя уровня olp
    """
    grant_olp_actions(group, organization, allow_actions)
    user.groups.add(group)
    assert user in group.user_set.all()
    assert has_object_permissions(user, organization, allow_actions)

    if deny_actions:
        assert has_object_permissions(user, organization, deny_actions) is False
