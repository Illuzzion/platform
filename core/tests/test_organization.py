import allure
import pytest
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist

from finances.exceptions import TariffError
from finances.models import TariffPlan
from finances.services.tariff_plan_svc import set_tariff_plan_for_user
from finances.services.user_finance_svc import replenishment_balance_by_administrator
from ..models import Organization
from ..services.organization_svc import (
    create_organization,
    add_student_to_organization,
    get_students_of_organization,
    is_user_in_organization,
    remove_student_from_organization,
    get_users_of_organization,
)

pytestmark = [pytest.mark.django_db]


def test_create_organization_normal(user: User, basic_tariff: TariffPlan):
    """Создание организации"""
    with allure.step("Создание организации"):
        org_title = "New"
        set_tariff_plan_for_user(user, basic_tariff)
        replenishment_balance_by_administrator(user, 100)

        organization = create_organization(org_title, user)

    with allure.step("Проверка корректности организации"):
        assert isinstance(organization, Organization)
        assert organization.title == org_title
        assert organization.master_user == user

        assert Group.objects.filter(name__contains=organization.id).exists()


def test_create_organization_without_tariff(user: User):
    """У пользователя не выбран тариф"""
    org_title = "New Organization"

    with pytest.raises(TariffError):
        create_organization(org_title, user)


def test_create_organization_balance_eq_0(user: User, basic_tariff: TariffPlan):
    """У пользователя на балансе 0"""
    org_title = "New Organization"
    set_tariff_plan_for_user(user, basic_tariff)

    with pytest.raises(TariffError):
        create_organization(org_title, user)


def test_create_organization_over_the_tariff_limit(
    user: User, basic_tariff: TariffPlan
):
    """
    Проверка,
    что организация сверх лимита тарифа не создается
    """
    org_title = "New"
    set_tariff_plan_for_user(user, basic_tariff)
    replenishment_balance_by_administrator(user, 100)

    create_organization(org_title, user)

    with pytest.raises(TariffError):
        create_organization("Organization over the tariff limit", user)


def test_create_organization_over_the_tariff_limit_when_inactive(
    user: User, basic_tariff: TariffPlan
):
    """
    Проверка, что организация создается, когда первая организация отключена
    """
    org_title = "New"
    set_tariff_plan_for_user(user, basic_tariff)
    replenishment_balance_by_administrator(user, 100)

    org = create_organization(org_title, user)
    org.active = False
    org.save()

    organization = create_organization("Organization over the tariff limit", user)
    assert isinstance(organization, Organization)
    assert organization.master_user == user


def test_normal_add_user_to_organization(organization: Organization, user) -> None:
    """
    Проверяем добавление пользователя в организацию
    должен быть выбран тариф и пополнен баланс, иначе нет возможности создать организацию
    """
    assert isinstance(organization, Organization)
    result = add_student_to_organization(user, organization)
    assert result


def test_exceptions_add_user_to_organization_on_user_delete(
    user: User, organization: Organization
) -> None:
    """Проверяем как контролируется добавление несуществующего пользователя"""
    user.delete()
    if not User.objects.filter(id=user.id):
        with pytest.raises(ObjectDoesNotExist):
            add_student_to_organization(user, organization)


def test_exception_add_user_to_organization_on_org_delete(
    user: User, organization: Organization
) -> None:
    """Проверяем как контролируется добавление несуществующей организации"""
    organization.delete()
    if not Organization.objects.filter(id=organization.id).exists():
        with pytest.raises(ObjectDoesNotExist):
            add_student_to_organization(user, organization)


def test_get_user_in_organization(user: User, organization: Organization) -> None:
    """
    Проверка есть ли user в списке студентов организации
    """
    result = add_student_to_organization(user, organization)
    assert result is True
    organization_users = get_students_of_organization(organization)
    assert user in organization_users


def test_is_user_in_organization(user: User, organization: Organization) -> None:
    """
    Проверка 2-х функций
     - is_student_in_organization
     - remove_student_from_organization
    """
    add_result = add_student_to_organization(user, organization)
    assert add_result is True
    assert is_user_in_organization(user, organization)

    remove_student_from_organization(user, organization)
    assert not is_user_in_organization(user, organization)


def test_get_users_of_organization(organization: Organization, user: User) -> None:
    """Проверка функции возвращающей количество пользователей в организации"""
    must_be_1 = get_users_of_organization(organization).count()
    assert must_be_1 == 1

    add_student_to_organization(user, organization)
    must_be_2 = get_users_of_organization(organization).count()
    assert must_be_2 == 2
