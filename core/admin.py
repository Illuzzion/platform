import logging
from typing import Sequence, Optional

from django.contrib import admin
from django.contrib.auth.models import User
from django.http import HttpRequest
from guardian.admin import GuardedModelAdmin
from guardian.utils import get_group_obj_perms_model, get_user_obj_perms_model

from core.models.organization import Organization
from core.models.organization_group import OrganizationGroup
from core.models.profession import Profession
from core.models.user_profile import UserProfile

logger = logging.getLogger(__name__)


GroupObjectPermission = get_group_obj_perms_model()
UserObjectPermission = get_user_obj_perms_model()


@admin.register(GroupObjectPermission)
class GroupObjectPermissionAdmin(admin.ModelAdmin):
    search_fields = ["name"]


@admin.register(UserObjectPermission)
class UserObjectPermissionAdmin(admin.ModelAdmin):
    list_display = ("user", "permission", "content_type", "object_pk")
    # list_filter = 'content_object',


@admin.register(Organization)
class OrganizationAdmin(GuardedModelAdmin):
    fields = (
        "id",
        "title",
        "master_user",
    )
    list_display = "title", "master_user", "active"
    search_fields = "title", "id"
    autocomplete_fields = ("master_user",)
    list_filter = (
        "master_user",
        "active",
    )

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return (
                "id",
                "title",
                "master_user",
            )
        return super().get_readonly_fields(request, obj)


admin.site.unregister(User)


class UserProfileInline(admin.StackedInline):
    model = UserProfile


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    search_fields = "username", "email"
    filter_horizontal = "groups", "user_permissions"
    ordering = ("username",)
    inlines = [UserProfileInline]

    fieldsets = (
        (
            "Общая информация",
            {
                "fields": (
                    "username",
                    "email",
                    ("date_joined", "last_login"),
                    ("is_active", "is_staff", "is_superuser"),
                )
            },
        ),
        (
            "Человек",
            {
                "description": "Информация о человеке",
                "fields": (
                    "last_name",
                    "first_name",
                ),
            },
        ),
        (
            "Дополнительная информация",
            {
                "classes": ("collapse",),
                "fields": (
                    "groups",
                    "user_permissions",
                    "password",
                ),
            },
        ),
    )


@admin.register(OrganizationGroup)
class OrganizationGroupAdmin(admin.ModelAdmin):
    search_fields = ("organization",)
    ordering = (
        "organization",
        "type",
    )
    list_filter = "organization", "type"
    list_display = (
        "organization",
        "type",
    )

    def get_readonly_fields(
        self, request: HttpRequest, obj: Optional = ...
    ) -> Sequence[str]:
        """Уберем возможность редактировать готовую запись"""
        if obj:
            return "organization", "type", "group"
        return []


@admin.register(Profession)
class ProfessionAdmin(admin.ModelAdmin):
    pass
