import logging

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, User
from django.db.models import QuerySet

from finances.services.tariff_plan_svc import get_tariff_plan
from .group_svc import add_user_to_group, get_organization_students_group
from ..models import Organization, GroupType

logger = logging.getLogger(__name__)


def create_organization(title: str, master_user: User) -> Organization | None:
    """
    Создание организации

    Перед созданием проверяется (в модели, через хуки):
    - может ли пользователь создать организацию.
    - проверяются лимиты на количество организаций, по тарифу.
    - проверка положительного баланса

    После создания, помимо организации, создаются
    - Группа студентов и связывается с организацией
    - группе назначаются права
    - создатель записывается в группу студентов
    - создается группа менеджеров и связывается с организацией
    - назначаются права на объект для группы менеджеров
    - создатель записывается в группу менеджеров
    """
    organization = Organization.objects.create(title=title, master_user=master_user)
    return organization


def count_of_organizations_user_has(user: User) -> int:
    """Получения количества организаций пользователя"""
    cnt = Organization.only_active.filter(master_user=user).count()
    return cnt


def add_student_to_organization(
    user: User, organization: Organization, check_existence=True
) -> bool:
    """
    Добавим студента к организации,
    так как связь m2m то нужно проверять существование
    """
    user_model = get_user_model()

    def _check_existence():
        """Если нет пользователя или организации вызовется исключение"""
        if check_existence:
            user_model.objects.get(id=user.id)
            Organization.objects.get(id=organization.id)

    try:
        _check_existence()

        group = get_organization_students_group(organization)

        add_user_to_group(user, group)
    except (Organization.DoesNotExist, user_model.DoesNotExist) as e:
        logger.exception("Exception in add_student_to_organization().")
        raise e
    except Group.DoesNotExist:
        logger.exception("Group does not exists")
        return False
    except Exception as e:
        print(e)
    else:
        return True


def get_users_of_organization(organization: Organization) -> QuerySet[User]:
    """Вернем всех пользователей организации"""
    users = User.objects.distinct().filter(
        groups__organization_groups__organization=organization
    )
    return users


def get_students_of_organization(organization: Organization) -> QuerySet[User]:
    """Вернем всех пользователей-студентов организации"""
    student_list = get_users_of_organization(organization).filter(
        groups__organization_groups__type=GroupType.STUDENTS
    )
    return student_list


def is_user_in_organization(user: User, organization: Organization) -> bool:
    """Проверка принадлежности студента к организации"""
    return get_users_of_organization(organization).filter(id=user.id).exists()


def remove_student_from_organization(user: User, organization: Organization) -> None:
    """Удалим студента из группы организации"""
    students_group = Group.objects.filter(
        organization_groups__organization=organization,
        organization_groups__type=GroupType.STUDENTS,
    ).get()
    students_group.user_set.remove(user)


def user_has_reached_organization_limit(user: User) -> bool:
    """
    Проверка достиг ли пользователь лимита на создание организации
    """
    tp = get_tariff_plan(user)

    if not tp:
        return True

    user_organization_count = count_of_organizations_user_has(user)
    return user_organization_count < tp.max_organization_count
