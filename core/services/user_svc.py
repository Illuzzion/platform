from django.contrib.auth.models import User
from django.db.models import QuerySet

from ..models import Organization


def organizations_user_belongs_to(user: User) -> QuerySet[Organization]:
    """Список организаций в которых состоит пользователь"""
    qs = Organization.objects.filter(
        organization_groups__group__in=user.groups.all()
    ).distinct()
    return qs
