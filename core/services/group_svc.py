from django.contrib.auth.models import Group, User
from django.db.models import QuerySet

from ..models import Organization, GroupType, OrganizationGroup


def generate_group_name(organization: Organization, prefix="", tail="") -> str:
    """
    Помощь при генерации названия для группы организации

    >>> generate_group_name(organization, prefix='<', tail='>')
    f'<{organization.title}-{organization.id}>'
    """
    return f"{prefix}{organization.title}-{organization.id}{tail}"


def create_group(name: str) -> Group:
    """Создание группы Django с именем name"""
    g, _ = Group.objects.get_or_create(name=name)
    return g


def add_user_to_group(user: User, group: Group):
    """Добавление пользователя в группу"""
    return user.groups.add(group)


def associate_organization_and_group(
    organization: Organization, group: Group, group_type: GroupType = GroupType.STUDENTS
) -> OrganizationGroup:
    """Создаем связь группы с организацией"""
    association, created = OrganizationGroup.objects.get_or_create(
        organization=organization, group=group, type=group_type
    )
    return association


def get_organization_groups(organization: Organization) -> QuerySet[Group]:
    """Вернем группы связанные с организацией"""
    groups_qs = Group.objects.filter(organization_groups__organization=organization)
    return groups_qs


def get_organization_students_group(organization: Organization) -> Group:
    """Получим группу студентов для организации"""
    groups = get_organization_groups(organization)
    student_group = groups.filter(organization_groups__type=GroupType.STUDENTS).get()
    return student_group


def get_organization_managers_group(organization: Organization) -> Group:
    """Получим группу менеджеров для организации"""
    groups = get_organization_groups(organization)
    student_group = groups.filter(organization_groups__type=GroupType.MANAGERS).get()
    return student_group


def get_users_of_group(group: Group) -> QuerySet[User]:
    """Вернем список пользователей группы"""
    users = User.objects.filter(groups=group)
    return users
