from typing import Iterable

from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.db.models import Model
from guardian.shortcuts import assign_perm


def build_mlp_actions(
    obj: Model, permission_list: Iterable[str], with_app_label=True
) -> list[str]:
    """Построение списка действий с объектом на уровне MLP"""
    model_options = obj._meta
    app_label = model_options.app_label
    model_name = model_options.model_name
    template = (
        "{app_label}.{action}_{model_name}"
        if with_app_label
        else "{action}_{model_name}"
    )

    actions_name = [
        template.format(app_label=app_label, action=action, model_name=model_name)
        for action in permission_list
    ]

    return actions_name


def get_instance_type(user_or_group: User | Group) -> tuple[User | None, Group | None]:
    """
    Вернем кортеж с пользователем или группой,
    в зависимости от типа аргумента user_or_group
    """
    if isinstance(user_or_group, User):
        return user_or_group, None

    if isinstance(user_or_group, Group):
        return None, user_or_group

    return None, None


def grant_mlp_permissions(
    user_or_group: User | Group, obj: Model, mlp_actions: Iterable[str]
) -> None:
    """Назначение прав на объект, на уровне MLP"""
    ct = ContentType.objects.get_for_model(obj)
    permissions = list(
        Permission.objects.filter(content_type=ct, codename__in=mlp_actions)
    )

    user, group = get_instance_type(user_or_group)
    if user:
        return user_or_group.user_permissions.add(*permissions)

    if group:
        return user_or_group.permissions.add(*permissions)

    return None


def build_olp_actions(obj: Model, permission_list: Iterable[str]) -> Iterable[str]:
    """Построение списка действий с объектом на уровне OLP"""
    return build_mlp_actions(obj, permission_list, with_app_label=False)


def grant_olp_permissions(
    who_gets_permissions: User | Group,
    object_of_permission: Model,
    mlp_actions: Iterable[str],
) -> None:
    """
    Присвоение прав на объект
    Пример mlp_actions:
        ['view_user'] - просмотр модели user
    """
    for permission in mlp_actions:
        assign_perm(
            permission, who_gets_permissions, object_of_permission
        )


def grant_mlp_actions(
    user_or_group: User | Group, obj: Model, actions: Iterable[str]
) -> None:
    """Даем права actions пользователю или группе на модель obj"""
    requested_permissions = build_mlp_actions(obj, actions, with_app_label=False)
    grant_mlp_permissions(user_or_group, obj, requested_permissions)


def grant_olp_actions(
    user_or_group: User | Group, obj: Model, actions: Iterable[str]
) -> None:
    """Даем права actions пользователю или группе на объект obj"""
    requested_permissions = build_olp_actions(obj, actions)
    grant_olp_permissions(user_or_group, obj, requested_permissions)


def has_model_permissions(
    user_or_group: User | Group, obj: Model, actions: Iterable[str]
) -> bool:
    """Проверка имеет или группа или пользователь права actions на модель obj"""
    permissions = build_mlp_actions(obj, actions)
    return user_or_group.has_perms(permissions)


def has_object_permissions(
    user_or_group: User | Group, obj: Model, actions: Iterable[str]
) -> bool:
    """Проверка имеет или группа или пользователь права actions на объект obj"""
    allowed_permissions = build_mlp_actions(obj, actions)
    return all(
        [user_or_group.has_perm(permission, obj) for permission in allowed_permissions]
    )
