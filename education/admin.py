from typing import Sequence

from django.contrib import admin

from education.models.edication_process import EducationProcess
from education.models.study_course import StudyCourse


@admin.register(StudyCourse)
class StudyCourseAdmin(admin.ModelAdmin):
    list_display = ("title",)
    search_fields = ("title",)


@admin.register(EducationProcess)
class EducationProcessAdmin(admin.ModelAdmin):
    list_display = "student", "course", "organization", "start", "end", "active"
    fields = (
        "student",
        ("course", "organization"),
        "active",
        ("start", "end"),
        "updated",
    )
    autocomplete_fields = (
        "student",
        "course",
        "organization",
    )
    list_filter = "active", "organization", "course"
    readonly_fields = ("updated",)
    search_fields = (
        "organization__title",
        "course__title",
        "student__username",
        "student__last_name",
        "student__email",
    )

    def get_readonly_fields(self, request, obj=None) -> Sequence[str]:
        """Пометим поля для чтения когда редактируем"""
        ro_fields = super().get_readonly_fields(request, obj)

        if obj:
            return ["organization", "student", "course"] + list(ro_fields)

        return ro_fields
