from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _

from .study_course import StudyCourse


class EducationProcess(models.Model):
    """
    Здесь только студенты
    """

    student = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="students",
        verbose_name=_("Student"),
    )

    course = models.ForeignKey(
        StudyCourse,
        on_delete=models.CASCADE,
        related_name="course_students",
        verbose_name=_("Course"),
    )

    organization = models.ForeignKey(
        "core.Organization",
        on_delete=models.CASCADE,
        related_name="organization_students",
        help_text=_("The organization that paid for the training"),
        verbose_name=_("Organization"),
    )

    active = models.BooleanField(
        default=True, help_text=_("Is this training active?"), verbose_name=_("Active?")
    )

    start = models.DateTimeField(_("Start education"))
    end = models.DateTimeField(_("End education"))

    updated = models.DateTimeField(_("Updated"), auto_now=True)

    class Meta:
        db_table = 'content"."education_processes'
        ordering = "organization", "updated"
        verbose_name = _("Education Process")
        verbose_name_plural = _("Education Processes")

    def __str__(self):
        return f"{self.student}, {self.course}"
