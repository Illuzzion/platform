from django.db import models
from django.utils.translation import gettext_lazy as _


class StudyCourse(models.Model):
    """
    Курсы для обучения
    пока заглушка
    """

    title = models.CharField(_("Title"), max_length=100)

    class Meta:
        db_table = 'content"."study_courses'
        ordering = "title", "id"
        verbose_name = _("Study Course")
        verbose_name_plural = _("Study Courses")

    def __str__(self):
        return self.title
