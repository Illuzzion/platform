# Установка для разработки

## `poetry`

```shell
pip install poetry
poetry install
```

## `pre-commit` хук

```shell
pre-commit install
```

# Документация

Начал делать документацию, запуск `mkdocs serve` или `make docs-server`
