import logging
from typing import Any

from django.contrib.auth import get_user_model

User = get_user_model()
logger = logging.getLogger(__name__)


def send_email_message(to_user: User, subject: str, body: str):
    logger.debug("Called send_email_message()")


def send_message(
        to_user: User, message_type, body: str, **kwargs: dict[str, Any]
):
    logger.debug("Called send_message()")


def send_message_by_template(
        to_user: User,
        message_type,
        body: str,
        template: str,
        **kwargs: dict[str, Any],
):
    logger.debug("Called send_message_by_template()")
