import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _
from model_utils.managers import InheritanceManager


class BaseUserMessage(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)

    from_user = models.ForeignKey(
        "auth.User", on_delete=models.CASCADE, related_name="+"
    )

    to_user = models.ForeignKey(
        "auth.User", on_delete=models.CASCADE, related_name="+"
    )

    body = models.TextField()

    sent = models.BooleanField(
        default=False, help_text=_("Sent flag (for message queue)")
    )

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    objects = InheritanceManager()

    class Meta:
        ordering = ("updated",)
        db_table = 'content"."base_messages'

    def __str__(self):
        return f"{self.from_user} -> {self.to_user}"
