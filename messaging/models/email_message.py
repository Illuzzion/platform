from django.db import models
from django.utils.translation import gettext_lazy as _

from messaging.models.base_user_message import BaseUserMessage


class EmailMessage(BaseUserMessage):
    subject = models.CharField(max_length=150)

    class Meta:
        db_table = 'content"."email_messages'
        ordering = ("id",)
        verbose_name = _("Email message")
        verbose_name_plural = _("Email messages")

    def __str__(self):
        return f"{self.to_user} -> {self.updated}"
