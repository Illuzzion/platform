from django.db import models
from django.utils.translation import gettext_lazy as _

from messaging.models.base_user_message import BaseUserMessage


class SiteMessage(BaseUserMessage):
    viewed = models.BooleanField(default=False)

    class Meta:
        db_table = 'content"."site_messages'
        ordering = ("id",)
        verbose_name = _("Site message")
        verbose_name_plural = _("Site messages")
