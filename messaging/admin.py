from django.contrib import admin

from messaging.models.email_message import EmailMessage
from messaging.models.site_message import SiteMessage


@admin.register(EmailMessage)
class EmailMessageAdmin(admin.ModelAdmin):
    fields = (
        "id",
        ("from_user", "to_user"),
        "subject",
        "body",
        "sent",
        ("created", "updated"),
    )
    autocomplete_fields = "to_user", "from_user"
    readonly_fields = "id", "created", "updated"
    list_display = "from_user", "to_user", "subject", "sent"


@admin.register(SiteMessage)
class SiteMessageAdmin(admin.ModelAdmin):
    fields = "id", ("from_user", "to_user"), "body", "sent", ("created", "updated")
    autocomplete_fields = "to_user", "from_user"
    readonly_fields = "id", "created", "updated"
    list_display = "from_user", "to_user", "sent"
