from django.contrib import admin
from django.urls import path, include

admin.site.site_header = "Учебная платформа"
admin.site.site_title = "Админка"
admin.site.index_title = "Администрирование"

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include("api.urls")),
]
